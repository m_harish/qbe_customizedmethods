/**
 *
 */
package com.cognizant.cognizantits.engine.reporting.sync.jiracloud;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.auth.BasicScheme;

import com.cognizant.cognizantits.engine.reporting.sync.BasicHttpClient;

/**
 * @author 576250
 *
 */
public class ZephyrHttpClient extends BasicHttpClient {

    private Map<String, String> OPTIONS = new HashMap<String, String>();

    public ZephyrHttpClient(URL url, String userName, String password, Map<String, String> config) throws MalformedURLException {
        super(new URL(config.get("ZephyrBaseURL")), userName, password, config);
        OPTIONS = config;
    }

    @Override
    public void setHeader(HttpGet httpget) {
        httpget.setHeader("zapiAccessKey", OPTIONS.get("AccessKey"));
        //httpget.setHeader("Content-Type", "application/json");
    }

    @Override
    public void setHeader(HttpPut httput) {
        httput.setHeader("zapiAccessKey", OPTIONS.get("AccessKey"));
        //httput.setHeader("Content-Type", "application/json");
    }

    @Override
    public void setHeader(HttpPost httppost) {
        httppost.setHeader("zapiAccessKey", OPTIONS.get("AccessKey"));
    }
    
    @Override
    public void auth(HttpRequest req) throws AuthenticationException {
        req.setHeader("Content-Type", "application/json");    	
    }
    
    @Override
    public void auth(HttpRequest req, String contentType) throws AuthenticationException {
        //req.setHeader("Content-Type", contentType);    	
    }
    
    
    @Override
    public void setPostEntity(File toUplod, HttpPost httppost) {
    	FileBody fileBody = new FileBody(toUplod, ContentType.DEFAULT_BINARY);
    	StringBody stringBody = new StringBody("Uploaded by CITS",ContentType.MULTIPART_FORM_DATA);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart("HTML Results",fileBody);
        httppost.setEntity(builder.build());
    }

}
